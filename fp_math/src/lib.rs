#![no_std]

mod fp64;

pub use fp64::Fp64;

pub const FP_SCALE: i64 = 32;

// Stupid windows things
#[cfg(target_os = "windows")]
#[used]
#[no_mangle]
pub static _fltused: i32 = 0;

#[cfg(target_os = "windows")]
#[no_mangle]
pub unsafe extern "C" fn memset(mem: *mut u8, _val: i32, _n: usize) -> *mut u8 {
    mem
}

#[cfg(target_os = "windows")]
#[no_mangle]
pub unsafe extern "C" fn memcpy(dest: *mut u8, _src: *const u8, _n: usize) -> *mut u8 {
    dest
}

#[cfg(target_os = "windows")]
#[no_mangle]
pub unsafe extern "C" fn memcmp(_mem1: *const u8, _mem2: *const u8, _n: usize) -> i32 {
    0
}

#[cfg(target_os = "windows")]
#[no_mangle]
pub extern "C" fn __CxxFrameHandler3() {
}

#[cfg(target_os = "windows")]
#[no_mangle]
pub extern "C" fn _DllMainCRTStartup() -> ! {
    panic!();
}
