# Installation
Install Rust (https://rustup.rs) and add to PATH

Ensure you're on the nightly toolchain. This can be changed by running 
`rustup override set nightly`.

Run the following command to build the library:
`cargo build -Z build-std=core,compiler_builtins --target x86_64-pc-windows-msvc --release`

The built lib files - `fp_math_bindings.dll`, `fp_math_bindings.dll.lib`, and 
`fp_math_bindings.lib` - can be found in target/x86_64-pc-windows-msvc/release.

`fp_math.h` is located in the fp_math_bindings/include directory and should work 
for either C or C++. In C++, everything is wrapped in the `fp64` namespace.


# Additional Notes
A `float` is `f32`, a `double` is `f64`. For `i32`, `i64`, and `u32`, use 
`int32_t`, `int64_t`, and `uint32_t` from stdint.h respectively.
